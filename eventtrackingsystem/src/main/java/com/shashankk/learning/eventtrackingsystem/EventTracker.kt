package com.shashankk.learning.eventtrackingsystem

import android.app.Application
import android.util.Log

import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * Created by Shashank on 18-09-2017.
 */

class EventTracker private constructor(val application: Application) {

    private val eventList: CopyOnWriteArrayList<Event> = CopyOnWriteArrayList()
    private val executorService: ExecutorService = Executors.newSingleThreadExecutor()

    init {
        application.registerActivityLifecycleCallbacks(object : ActivityCallbacks() {
            override fun onAppMinimized() {
                Log.i(TAG,"App minimized, Clearing Events")
                eventList.clear()
            }
        })
    }

    companion object {

        val TAG = javaClass.name

        @JvmStatic
        var instance: EventTracker? = null
            private set

        @JvmStatic
        fun init(application: Application) {
            if (instance == null) {
                synchronized(EventTracker::class.java) {
                    if (instance == null) {
                        instance = EventTracker(application)
                    }
                }
            }
        }
    }


    fun logEventStart(eventName: String, screenName: String) {
        val e = Event(eventName, screenName)
        e.startTimeStamp = System.nanoTime()
        executorService.submit(EventLogCallable(e))
    }

    fun logEventEnd(eventName: String, screenName: String) {
        val e = Event(eventName, screenName)
        e.endTimeStamp = System.nanoTime()
        executorService.submit(EventLogCallable(e))
    }

    fun addEvent(event: Event) {
        eventList.add(event)
    }

    fun getEventExecutionTime(eventName: String, screenName: String) =
            getEvents { it.id == eventName + "_" + screenName && !(it.startTimeStamp == 0L || it.endTimeStamp == 0L) }.last().let {
                (it.endTimeStamp - it.startTimeStamp) / 1000000000
            }


    fun getEvents(function: (Event) -> Boolean) = eventList.filter(function)


}
