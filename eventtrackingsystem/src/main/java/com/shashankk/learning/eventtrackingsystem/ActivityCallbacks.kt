package com.shashankk.learning.eventtrackingsystem

import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.os.Handler

/**
 * Created by Shashank on 19-09-2017.
 */
abstract class ActivityCallbacks : Application.ActivityLifecycleCallbacks {

    private val delay = 2000L

    private val handler = Handler()

    private val runnable = Runnable {
        onAppMinimized()
    }

    abstract fun onAppMinimized()

    override fun onActivityPaused(activity: Activity?) {
        handler.postDelayed(runnable, delay)
    }

    override fun onActivityResumed(activity: Activity?) {
        //If an activity is launched before timeout cancel events removal
        handler.removeCallbacks(runnable)
    }

    override fun onActivityStarted(activity: Activity?) {
    }

    override fun onActivityDestroyed(activity: Activity?) {
    }

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
    }

    override fun onActivityStopped(activity: Activity?) {
    }

    override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
    }
}