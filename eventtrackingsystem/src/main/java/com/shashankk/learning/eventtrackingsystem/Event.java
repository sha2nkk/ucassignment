package com.shashankk.learning.eventtrackingsystem;

import java.util.Map;

/**
 * Created by Shashank on 18-09-2017.
 */

public class Event {


    public final String eventName;
    public long startTimeStamp;
    public long endTimeStamp;
    public final String id;


    public Event(String eventName) {
        this(eventName, "");
    }

    public Event(String eventName, String screenName) {
        this.eventName = eventName;
        id = eventName +"_"+ screenName;
    }


}
