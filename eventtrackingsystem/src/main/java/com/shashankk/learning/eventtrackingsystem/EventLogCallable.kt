package com.shashankk.learning.eventtrackingsystem


import android.util.Log
import java.util.concurrent.Callable

/**
 * Created by Shashank on 18-09-2017.
 */

class EventLogCallable(private val event: Event) : Callable<Void> {

    @Throws(Exception::class)
    override fun call(): Void? {

        val tracker = EventTracker.instance
        event.apply {
            if (endTimeStamp == 0L) {
                tracker?.addEvent(this)
            } else if (event.startTimeStamp == 0L) {
                //find Existing non-Complete Event
                val existingEvent = tracker?.getEvents { it.id == id && it.endTimeStamp == 0L }?.last()
                existingEvent?.endTimeStamp = endTimeStamp
                if(existingEvent!= null && existingEvent.startTimeStamp!=0L)
                    Log.i(EventTracker.TAG, eventName + " duration : " + (existingEvent.endTimeStamp - existingEvent.startTimeStamp)/100000 + " ms")
            }
        }


        return null
    }
}
