package com.shashankk.ucassignment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.shashankk.learning.eventtrackingsystem.EventTracker;
import com.shashankk.ucassignment.databinding.ActivityCityBinding;

import java.io.IOException;
import java.util.List;

public class CityActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityCityBinding binding;
    Snackbar errorSnackBar;
    LocationManager locationManager;
    String cityName;

    public final String[] PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_city);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        binding.btnCity.setOnClickListener(this);

        getLocation();
        errorSnackBar = Snackbar.make(binding.getRoot(), "Location Permission Granted", Snackbar.LENGTH_INDEFINITE);
        errorSnackBar.setAction("Retry", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermissions();
            }
        });

    }

    public void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 101) {
            if (permissions.length != grantedPermissionCount(grantResults)) {
                errorSnackBar.show();
                binding.btnCity.setVisibility(View.GONE);
                binding.txtCityName.setVisibility(View.GONE);
            } else {
                binding.btnCity.setVisibility(View.VISIBLE);
                binding.txtCityName.setVisibility(View.VISIBLE);
                getLocation();
            }
        }
    }

    public int grantedPermissionCount(@NonNull int[] grantResults) {
        int count = 0;
        for (int i = 0; i < grantResults.length; i++) {
            if (grantResults[i] != -1)
                count++;
        }
        return count;
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
            return;
        }
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Geocoder gcd = new Geocoder(this);
        try {
            List<Address> addressList = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (addressList.size() > 0) {
                cityName = addressList.get(0).getLocality();
            }
        } catch (Exception e) {
            e.printStackTrace();
            cityName = "Cannot Load City Name";
        } finally {
            binding.txtCityName.setText(cityName);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCity:
                EventTracker.getInstance().logEventStart("TimeTakenLoadCityImage", "imageLoad");
                Intent cityImageIntent = new Intent(this, CityImageActivity.class);
                startActivity(cityImageIntent);
                break;
        }
    }
}
