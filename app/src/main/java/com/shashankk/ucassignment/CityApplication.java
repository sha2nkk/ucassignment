package com.shashankk.ucassignment;

import android.app.Application;

import com.shashankk.learning.eventtrackingsystem.EventTracker;

/**
 * Created by Shashank on 19-09-2017.
 */

public class CityApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        EventTracker.init(this);
    }
}
