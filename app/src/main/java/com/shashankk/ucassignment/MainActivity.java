package com.shashankk.ucassignment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.shashankk.learning.eventtrackingsystem.EventTracker;
import com.shashankk.ucassignment.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.btnShowCities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cityActivityIntent = new Intent(MainActivity.this, CityActivity.class);
                startActivity(cityActivityIntent);
                EventTracker.getInstance().logEventStart("TimeTakenShowCityPage", "cityImage");
            }
        });
    }
}
